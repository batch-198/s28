// db.users.insertOne({
// 
//     "username": "kangSeulgi",
//     "password": "kangSseul"    
// 
//     })

// db.users.insertOne({
// 
//     "username": "sonSeungwan",
//     "password": "seungwannie"
// 
// })

// insert multiple documents at once
// db.users.insertMany (
// 
//     [
//         {
//             "username": "parkSooyoung",
//             "password": "parkSoo"
//         },
//         {
//             "username": "kimYerim",
//             "password": "yerimmie"
//         }
//     ]
// )

// db.products.insertMany (
//     [
//         {
//             "name": "Red Flavor Normal Edition",
//             "description": "3rd Mini Album, Regular Edition",
//             "price": 1500
//         },
//         {
//             "name": "Red Flavor Limited Edition A",
//             "description": "3rd Mini Album, Limited Edition with exclusive documentary",
//             "price": 2500
//         },
//         {
//             "name": "Red Flavor Limited Edition B",
//             "description": "3rd Mini Album, Limited Edition with exclusive documentary",
//             "price": 2500
//         }
//     ]
// )
        
//Read/Retrieve
 
// db.collections.find() - return/finds all documents in the collection
// db.users.find()
        
// db.collection.find({"criteria":"value"}) - return/finds all the documents 
// that match the criteria
// db.users.find({"username": "kangSeulgi"})
// 
// db.cars.insertMany(
//     [
//        {
//          "name":"Vios",
//          "brand":"Toyota",
//          "type": "sedan",
//          "price": 1500000
//        },
//        {
//          "name":"Tamaraw FX",
//          "brand":"Toyota",
//          "type": "auv",
//          "price": 750000
//        },
//        {
//          "name":"City",
//          "brand":"Honda",
//          "type": "sedan",
//          "price": 1600000
//        }
//     ]
// )

// db.cars.find({"type":"sedan"})
// db.cars.find({"brand":"Toyota"})

// db.collection.findOne({}) - find/return the first item/document in the collection
// db.cars.findOne({})

// db.collection.findOne({"criteria":"value"}) - find/return the first item/document that matches the criteria
// db.cars.findOne({"type":"sedan"})

// Update
// db.collection.updateOne({"criteria":"value},{$set:("fieldtoBeUpdated":"Updated Value"}})
// Allows us to update the item that matches the criteria
// db.users.updateOne({"username":"kangSeulgi"},{$set:{"username":"seulKkang99"}})

// db.collection.updateOne({},{$set:{"fieldToBeUpdated":"Updated Value"}})
// Allows us to update the first item in the collection
// db.users.updateOne({},{$set:{"username":"updatedUsername"}})

// if the field being updated does not yet exist, mongoDB will instead add the field into the document
// db.users.updateOne({"username":"ireneBae"},{$set:{"isAdmin":true}})

// db.collection.updateMany({},{$set:{"fieldToBeUpdated":"Updated Value"}})
// Allows us to update all items in the collection
// db.users.updateMany({},{$set:{"isAdmin":true}})

// db.collection.UpdateMany({"criteria":"value"},{$set:{"fieldToBeUpdated":"Updated Value"}})
// Allows us to update all items that match our criteria
// db.cars.updateMany({"type":"sedan"},{$set:{"price":1000000}})

// Delete

// db.collection.deleteOne({})
// db.products.deleteOne({}) - deletes first item in collection

// db.collection.deleteOne({"criteria":"value"}) - deletes the first item that matches the criteria
// db.cars.deleteOne({"brand":"Toyota"})

// db.users.deleteMany({"isAdmin":true})

// db.collection.deleteMany({}) - deletes all items in the query
// db.products.deleteMany({})
db.cars.deleteMany({})